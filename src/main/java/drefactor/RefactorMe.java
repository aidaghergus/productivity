package drefactor;

import cnavigation.CognyteEmployee;

import java.util.HashMap;
import java.util.Map;

public class RefactorMe {

    //1. Ctrl+Alt+L Reformat code
    //2. Shift+F6 Rename
    //3. Ctrl+Alt+V Introduce variable
    //4. Ctrl+Alt+N Inline
    //5. Ctrl+Alt+M Extract method
    //6. F6 Move
    //7. Ctrl+Alt+Shift+T Refactor this
    //8. Alt+Delte Safe delete

    public static void main(String[] args) {

        System.out.println("Safe reformat and restructure existing code using IntelliJ built in features");

        Map<String, Double> ben = new HashMap<>();
        ben.put("Benefits1",125.0);
        ben.put("Benefits2", 678.0);
        ben.put("Benefits3",912.0);

        CognyteEmployee cognyteEmployee = new CognyteEmployee("John","Doe",20,12345,ben);

        //calculate total benefit
        //total benefit
        double totalBenefit=0.0;
          for (double i:cognyteEmployee.getBenefits().values())
              totalBenefit+=i;

        //calc net salary
        double netSalary;
        netSalary=cognyteEmployee.getSalary()-cognyteEmployee.getSalary()*0.25-cognyteEmployee.getSalary()*0.1- cognyteEmployee.getSalary()*0.025;

        //calc total revenue
        double totalRevenue=totalBenefit+netSalary;
        System.out.println(totalRevenue);
    }
}
