package cnavigation;

public class NavigateMe {

    //1. Ctrl + Click -> goes everywhere
    //2. Ctrl + Alt + Left/Right -> Navigate back/forward; Alt + Left + Right -> Navigates through tabs
    //3. Ctrl + E -> recent files; Ctrl+ Shift + E -> recent locations
    //4. Shiftx2 -> Search everywhere
    //5. Ctrl + N -> go to class
    //6. Ctrl + Shift + F -> Find in files
    //7. Ctrl+G -> Go to line
    //8. F2 -> goes to next error; Shift + F2 -> goes to previous error
    //9. Ctrl+F12 -> List methods
    //10. Alt + Up/Down -> Scrolls through methods
    //11. Alt+1 -> Opens "Project"

    public static void main(String[] args) {
        System.out.println("Navigate easier through code; quickly find points of interest using mouse and keyboard.");
    }
}
