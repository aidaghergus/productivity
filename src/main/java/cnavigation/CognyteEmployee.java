package cnavigation;

import bcompletion.Employee;
import lombok.Data;

import java.util.Map;

@Data
public class CognyteEmployee extends Employee {
    private Map<String, Double> benefits;

    public CognyteEmployee(String firstName, String lastName, int age, double salary, Map<String, Double> benefits) {
        super(firstName, lastName, age, salary, false);
        this.benefits = benefits;
    }
}
