package aselection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person implements Serializable {
    private String firstName;
    private String lastName;
    private Integer age;

    public void printConsent() {
        System.out.printf("I, %s %s have read and understand the information provided.", firstName, lastName);
    }

    public String getConsent() {
        return isAdult() ?
                "I have read and I " +
                        "understand the provided information and have had the opportunity to ask questions. " +
                        "I understand that my participation is voluntary and that I am free to withdraw at any time, without giving a reason and without cost. I understand that I will be given a copy of this consent form. I voluntarily agree to take part in this study. " :
                null;
    }

    public boolean isAdult() {
        return age > 18;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeChars(firstName);
        out.writeChars(lastName);
        out.writeInt(age);
    }
}
