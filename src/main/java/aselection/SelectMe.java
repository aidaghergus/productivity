package aselection;

public class SelectMe {

    //1.Ctrl + W -> Extends Selection
    //2.Ctrl + Shift + W -> Shrink Selection
    //3.Shift + Left/Right; Ctrl + Shift + Left/Right -> Extend selection with one character/word
    //4.Click+Shift+Click - Select area
    //5.Ctrl + Shift + [/] -> Select until code block start/end
    //6.Ctrl + D -> Duplicates the line
    //7.Ctrl + C -> Copies the line
    //8.Ctrl + X -> Cuts the line
    //9.Ctrl + V -> Copies back the line
    //10.Ctrl + Z -> Undo
    //11.Ctrl + Y -> Deletes line
    //12.Ctrl + Shift + J - Smart joins lines
    //13.Ctrl + Enter - Smart line split
    //13.Alt + Shift + Up/Down -> moves line up/down
    //14.Ctrl + Shift + Up/Down -> moves statement up/down
    //15.Alt + Shift + Insert - activates column mode, arrows/Shift+arrows to jump up/right, Alt + Shift + Click to select random columns
    //16.Alt + J - finds occurrence and activates multiple cursors, Alt + Shift + J deactivate previous cursor
    //17.Ctrl + Alt + Shift +J - Select all occurrences

    public static void main(String[] args) {
        System.out.println("Shortcuts to select and manipulate words, lines, blocks etc.");
    }
}
