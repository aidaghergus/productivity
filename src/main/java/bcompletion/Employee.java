package bcompletion;

import aselection.Person;

public class Employee extends Person implements Workable {
    private Double salary;
    private Boolean taxation;

    public Employee() {
    }

    public Employee(String firstName, String lastName, int age, double salary, boolean taxation) {
        super(firstName, lastName, age);
        this.salary = salary;
        this.taxation = taxation;
    }

    public Double getSalary() {
        return salary;
    }

    public Boolean getTaxation() {
        return taxation;
    }

    @Override
    public void work() {
        System.out.println("I work");
    }
}
