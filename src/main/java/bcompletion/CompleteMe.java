package bcompletion;

public class CompleteMe {

    //1. Alt + Enter -> generates code when errors
    //2. Alt + Insert -> Generate
    //3. Ctrl + I - implement methods
    //4. Ctrl + O - override methods
    //5. Ctrl + P -> Show parameters
    //6. Ctrl + J -> see live templates - prefix and postfix completion
    //7. Ctrl + Shift +Enter -> completes the line
    //8. Ctrl + Space -> suggetions of name/methods x2 shows also the static methods
    //9. Ctrl + Shift + Space -> smart completion, only relevant suggestions (that match the type)
    //10. Tab vs Enter Completion
    //11. Ctrl + Alt + T -> surround with
    //12. Camel case Prefix completion, e.g:CWAS, OOME

    public static void main(String[] args) {
        System.out.println("Tips & tricks for code generation, error handling, use of live templates and code suggestions.");
    }
}