# README #

### Motivation ###

* Why choosing the long road? Why spending time on something that can be done faster?   
I chose the topic of my presentation with the desire of showing that there is a shorter way and to encourage everyone to explore the Keymap Reference and to take advantage of all these productivity features that are available.
Being productive is leveraging the resources you have to accomplish more with less effort, so don't blame your tool, just take the best out of it!

### Who is this for? ###
* Java Developers of any seniority.